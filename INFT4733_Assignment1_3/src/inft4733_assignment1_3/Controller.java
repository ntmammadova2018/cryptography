/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inft4733_assignment1_3;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.util.Duration;
/**
 *
 * @author nella
 */
public class Controller {
    private int sleepTime = 0;
    private int secretKey;    
    Timeline timeline,timeline2,timeline3,timeline4,timeline5,timeline6,timeline7;
    
    //UI elements
    @FXML
    public TextField primeInput;
    @FXML
    public TextField generatorInput;
    @FXML
    public TextField plaintextInput;
    @FXML
    Label lblprime;
    @FXML
    Label lblgenerator;    
    @FXML
    Label lbltext;
    @FXML
    public TextFlow result;
    @FXML
    public TextFlow result2;
    @FXML
    public Button connect;
    @FXML
    public Button send;
    @FXML
    public Button next;
    @FXML
    public Button prev;
    
    /**
     * @summary is called whenever the connect button is clicked
     *          displays the results of the first part of the assignment
     */
    public void connect(){
        //Clearing UI
        result.getChildren().clear();
        setTime(0);
        
        String primeS = primeInput.getText();
        String generatorS = generatorInput.getText();
        try{
            int prime = Integer.parseInt(primeS);
            int generator = Integer.parseInt(generatorS);
            if(generator >= prime){
                throw new Exception();
            }
            
            KeysGenerator keygenerator = new KeysGenerator();
            
            //Generate keys for Alice
            int[] AliceKeys = keygenerator.keysGenerator(prime, generator);
            timeline = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        Text t1 = new Text("Your private key: "+AliceKeys[0]+"\n");
                        t1.setFill(Color.web("#00ff00"));
                        result.getChildren().add(t1);
                    }));
            timeline.play();
            
            timeline2 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        Text t1 = new Text("Your public key: "+AliceKeys[1]+"\n");
                        t1.setFill(Color.web("#00ff00"));
                        result.getChildren().add(t1);
                    }));
            timeline2.play();
            

            //Generate keys for Bob
            int[] BobKeys = keygenerator.keysGenerator(prime, generator);
            
            timeline3 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        Text t1 = new Text("(Bob's private key): "+BobKeys[0]+"\n");
                        t1.setFill(Color.web("#00ff00"));
                        result.getChildren().add(t1);
                    }));
            timeline3.play();
            
            timeline4 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        Text t1 = new Text("You received Bob's public key: "+BobKeys[1]+"\n");
                        t1.setFill(Color.web("#00ff00"));
                        result.getChildren().add(t1);
                    }));
            timeline4.play();
            
            //Calculate secret key for AES-128 encryption
            timeline5 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        Text t1 = new Text("Calculating secret key...\n");
                        t1.setFill(Color.web("#00ff00"));
                        result.getChildren().add(t1);
                    }));
            timeline5.play();
            secretKey = (int) Math.pow(BobKeys[1], AliceKeys[0])%prime;
            timeline6 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {                        
                        Text t1 = new Text("Secret key: "+secretKey+"\n");
                        t1.setFill(Color.web("#00ff00"));
                        t1.setFont(Font.font ("Verdana", 16));
                        result.getChildren().add(t1);
                    }));
            timeline6.play();
            
            timeline7 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {                                          
                        next.setVisible(true);
                    }));
            timeline7.play();
        }
        catch(Exception ex){
            Text t0 = new Text("Please, follow the instructions");
            t0.setFill(Color.RED);
            timeline.stop();timeline2.stop();timeline3.stop();
            timeline4.stop();timeline5.stop();timeline6.stop();
            timeline7.stop();
            
            //Clearing UI
            result.getChildren().clear();
            result.getChildren().add(t0);
        }
    }
    
    /**
     * @summary is called whenever the send button is clicked
     *          displays the results of the second part of the assignment
     */
    public void aesCalc(){
        try{    
                //Clearing UI
                result2.getChildren().clear();
                setTime(0);
                
                AES128 aes = new AES128();
                
                String plaintext = plaintextInput.getText();
                if(plaintext == null || plaintext.isEmpty() || plaintext.equals("")){
                    throw new Exception();
                }

                //Encryption process
                String encryptedText = aes.encryption(plaintext, ""+secretKey);
                timeline = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        String IV = aes.IVtext.substring(0, aes.IVtext.length()-2);
                        Text t1 = new Text("IV: "+IV+"\n");
                        t1.setFill(Color.web("#00ff00"));
                        t1.setFont(Font.font ("Verdana", 16));
                        result2.getChildren().add(t1);
                    }));
                timeline.play();
                
                timeline2 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        String cipherText = aes.EncryptedText.substring(0, aes.EncryptedText.length()-2);
                        Text t1 = new Text("Ciphertext: "+cipherText+"\n");
                        t1.setFill(Color.web("#00ff00"));
                        t1.setFont(Font.font ("Verdana", 16));
                        result2.getChildren().add(t1);
                    }));
                timeline2.play();
                
                timeline3 = new Timeline(new KeyFrame(
                    Duration.millis(getTime()),
                    ae -> {
                        Text t1 = new Text("Bob has received the encrypted message.\n");
                        Text t2 = new Text("Congratulations!");
                        t1.setFill(Color.web("#00ff00"));
                        t2.setFill(Color.web("#00ff00"));
                        t2.setFont(Font.font ("Verdana", 16));
                        result2.getChildren().add(t1);
                        result2.getChildren().add(t2);
                    }));
                timeline3.play();                

            }
            catch(Exception ex){
                System.out.println("Oops! Error: "+ex);
                Text t0 = new Text("Please, follow the instructions");
                t0.setFill(Color.RED);
                
                //Clearing UI
                result2.getChildren().clear();
                result2.getChildren().add(t0);
            }
    }
    
    /**
     * @summary Adds milliseconds for threads to wait
     * @return {Integer}
     */
    public int getTime(){
        sleepTime += 1000;
        return sleepTime;
    }
    
    /**
     * @param time given integer to set for sleeping time of threads
     * @summary Assigns the milliseconds to sleeping time of threads
     */
    public void setTime(int time){
        sleepTime = time;
    }
    
    /**
     * @summary is called whenever the next button is clicked
     *          changes the layout to the second part of the assignment
     */
    public void changeLayout(){
        lblprime.setVisible(false);
        lblgenerator.setVisible(false);
        primeInput.setVisible(false);
        generatorInput.setVisible(false);
        connect.setVisible(false);
        result.getChildren().clear();
        lbltext.setVisible(true);
        plaintextInput.setVisible(true);
        send.setVisible(true);
        prev.setVisible(true);
        
        next.setVisible(false);
    }
    
    /**
     * @summary is called whenever the previous button is clicked
     *          changes the layout to the first part of the assignment
     */
    public void changeLayoutBack(){
        lblprime.setVisible(true);
        lblgenerator.setVisible(true);
        primeInput.setVisible(true);
        generatorInput.setVisible(true);
        connect.setVisible(true);
        result2.getChildren().clear();
        lbltext.setVisible(false);
        plaintextInput.setVisible(false);
        send.setVisible(false);
        
        prev.setVisible(false);
    }
}

