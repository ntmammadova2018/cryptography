package inft4733_assignment1_3;

import java.util.Scanner;

/**
 *
 * @author nella
 */
public class KeysGenerator {

    /**
     * @param prime prime number for the calculation
     * @param generator generator number for the calculation
     * @summary given input, generates private and public key
     * @return {Array}
     */
    public static int[] keysGenerator(int prime, int generator) {
        //an array for keys
        int keys[] = new int[2];
        
        //Private key generation
        boolean valid = false;
        int KEYpr = 0;
        while(!valid){
            KEYpr = (int) (Math.random()*1000)%prime;
            if(KEYpr != 0)
                valid = true;
        }
        keys[0] = KEYpr;
        
        //Public key generation
        valid = false;
        int KEYpu = 0;
        while(!valid){
            KEYpu = ((int) Math.pow(generator, KEYpr))%prime;
            if(KEYpu != 0)
                valid = true;
        }
        keys[1] = KEYpu;
        
        //Return both keys in the array
        return keys;
    }    
}
