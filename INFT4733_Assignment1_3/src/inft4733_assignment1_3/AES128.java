/*
 * Reference to: https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html#getIV()
 * Reference to: https://stackoverflow.com/questions/28025742/encrypt-and-decrypt-a-string-with-aes-128
 */
package inft4733_assignment1_3;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Encoder;

/**
 *
 * @author nella
 */
public class AES128 {

private static Cipher cipher;
private static byte[] IV;
public static String IVtext;
public static String EncryptedText;
public static String DecryptedText;
private static int sleepTime = 1000;

    public AES128(){
        try{
            //Creating new AES instance
            cipher = Cipher.getInstance("AES/ECB/NoPadding");
            
            //Creating random initial vector
            SecureRandom random = new SecureRandom();
            byte[] ivRandomBytes = new byte[16];
            random.nextBytes(ivRandomBytes);
            IV = ivRandomBytes;
            //IV = new IvParameterSpec(ivRandomBytes);
        }
        catch(Exception ex){
            System.out.println("Oops! Error: "+ex.getMessage());
        }
    }
    
    /**
     * @param s given string for padding
     * @summary Appends new characters to the initial string till reached the limit
     * @return {String}
     */
    private static String addPaddingKey(String s){
        int i = 0;
        while(s.length()%16 != 0){
            s += s.substring(i,i+1);
            i++;
        }
        return s;
    }
    
    /**
     * @param plainText String input
     * @summary given input, adds padding to a plaintext if length of plaintext
     * is not divisible by 16
     * @return {String}
     */
    public static String addPadding(String plainText) {

        StringBuilder sb = new StringBuilder(plainText);
        int length = plainText.length();

        if (length % 16 != 0) {
            for (int i = 0; i < 16 - (length % 16); i++) {
                sb.append("x");
            }
        }
        return sb.toString();
    }
    
    /**
     * @param plainText String input
     * @summary given input, implements CBC mode
     */
    public static String encryption(String plainText, String key) {
        byte[] previous = IV;
        
        plainText = addPadding(plainText); // adds padding if necessary
        
        //Making ready the key for encryption
        key = addPaddingKey(key);
        Key aesKey = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
        
        StringBuilder encryptedText = new StringBuilder("");

        System.out.println("Plain text: " + plainText);

        byte[] plainTextBytes = plainText.getBytes(StandardCharsets.UTF_8);
        int plainLength = plainText.length();

        int blockNumber = 0;
        int numberOfBlocks = (int) Math.ceil(plainLength / 16);

        byte[] XORedByteArray = new byte[16];
        while (blockNumber < numberOfBlocks) {
            int blockSize = 16; // 16 byte
            if (plainLength - (blockNumber * 16) < 16) {
                blockSize = plainLength - (blockNumber * 16);
            }

            int m = 0;
            for (int j = blockNumber * 16; j < (blockNumber * 16) + blockSize; j++) {
                int XORedByte = plainTextBytes[j] ^ previous[m];  //XORing
                XORedByteArray[m] = (byte) XORedByte;
                m++;
            }

            blockNumber++;
            previous = subencryption(XORedByteArray, aesKey);

            System.out.println("Block " + blockNumber + ": " + Base64.getEncoder().encodeToString(previous));
            encryptedText.append(Base64.getEncoder().encodeToString(previous));
        }
        
        IVtext =  Base64.getEncoder().encodeToString(IV);
        EncryptedText = encryptedText.toString();
        
        return EncryptedText;
    }
    
     /**
     * @param text plaintext wich will be encrypted
     * @param key a secret key which will be used for encryption
     * @summary Encrypts the plaintext with given key according to AES algorithm
     * @return {Array}
     */
    public static byte[] subencryption(byte[] text, Key key){
        byte[] encryptedText = ("").getBytes(StandardCharsets.UTF_8);
        
        try{
            //Encryption of the text
            cipher.init(Cipher.ENCRYPT_MODE, key);
            encryptedText = cipher.doFinal(text);            
                       
        }catch(Exception ex){
            System.out.println("Oops! Error: "+ex.getMessage());
        }
         
        return encryptedText;
    }
}
